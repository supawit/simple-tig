To use this stack you have to create 2 files.

1. create **.env** file that declare environment variables for docker-compose like
```
INFLUXDB_DB=monitor
INFLUXDB_ADMIN_USER=admin
INFLUXDB_ADMIN_PASSWORD=veryverrysecurepassword
INFLUXDB_USER=monitor
INFLUXDB_USER_PASSWORD=anotherveryverrysecurepassword
```

2. create **.htpasswd** file for nginx basic authentication. you can use tool like htpasswd
```
$ htpasswd -c .htpasswd admin
```

after create 2 files then let's
```
docker-compose up -d
```

have a nice day.